package com.proyecto;

import java.util.ArrayList;

public class TutoArrayList {
	
	public void compara(int numeroVeces) {
		//////////////////////////////////////////////////////////////////////////////////////////////////
		//																								//
		//										   Definido												//
		//																								//
		//////////////////////////////////////////////////////////////////////////////////////////////////
		
		ArrayList<Integer> ArrayDef = new ArrayList<Integer>(numeroVeces);
		long inicio1 = System.currentTimeMillis();
		for(int i=0;i<numeroVeces; i++) {
			ArrayDef.add(i);
		}
		
		long fin1 = System.currentTimeMillis();
		long diferencia1 = fin1 - inicio1;
		
		//////////////////////////////////////////////////////////////////////////////////////////////////
		//																								//
		//										No Definido												//
		//																								//
		//////////////////////////////////////////////////////////////////////////////////////////////////
		
		long inicio2 = System.currentTimeMillis();
		ArrayList<Integer> ArrayNoDef = new ArrayList<Integer>();
		
		for(int i=0;i<numeroVeces; i++) {
			ArrayNoDef.add(i);
		}
		
		long fin2 = System.currentTimeMillis();
		long diferencia2 = fin2 - inicio2;
		
		System.out.println("El tiempode ArrayDef fue de: "+ diferencia1 + "\nEl Tiempo de ArrayNoDef fue de "+ diferencia2);

		
	}

}
