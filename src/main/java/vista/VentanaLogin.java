package vista;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Rectangle;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JPasswordField;

public class VentanaLogin extends JFrame {

	private JPanel contentPane;
	private JPasswordField campoPass;
	private Dimension tamPantalla;
	private Rectangle pantalla;

	/**
	 * Create the frame.
	 */
	public VentanaLogin() {
		tamPantalla=Toolkit.getDefaultToolkit().getScreenSize();
		pantalla=new Rectangle(tamPantalla);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//		setBounds(100, 100, 450, 376);
		setBounds(pantalla);
		setTitle("login");
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblLogin = new JLabel("Login");
		lblLogin.setHorizontalAlignment(SwingConstants.CENTER);
		lblLogin.setBounds(10, 11, 414, 14);
		contentPane.add(lblLogin);
		
		JLabel lblUsuario = new JLabel("Usuario");
		lblUsuario.setBounds(40, 107, 58, 14);
		contentPane.add(lblUsuario);
		
		JLabel pass = new JLabel("Password");
		pass.setBounds(40, 142, 58, 14);
		contentPane.add(pass);
		
		JButton botonAceptar = new JButton("Ingresar");
		botonAceptar.setBounds(166, 240, 89, 23);
		contentPane.add(botonAceptar);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Seleccione", "Administrador", "Usuario Basico"}));
		comboBox.setSelectedIndex(0);
		comboBox.setBounds(183, 104, 153, 20);
		contentPane.add(comboBox);
		
		campoPass = new JPasswordField();
		campoPass.setBounds(183, 139, 153, 20);
		contentPane.add(campoPass);
		
	}
}
