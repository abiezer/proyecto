package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JButton;

public class VentanaPrincipal extends JFrame {

	private JPanel contentPane;

	/**
	 * Create the frame.
	 */
	public VentanaPrincipal() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		setTitle("Ventana Principal");
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel miPanel = new JPanel();
		miPanel.setBounds(12, 12, 410, 237);
		contentPane.add(miPanel);
		miPanel.setLayout(null);
		
		JLabel lblBienvenido = new JLabel("Bienvenido");
		lblBienvenido.setHorizontalAlignment(SwingConstants.CENTER);
		lblBienvenido.setBounds(12, 12, 386, 16);
		miPanel.add(lblBienvenido);
		
		JButton btnConsultar = new JButton("Consultar");
		btnConsultar.setBounds(257, 96, 98, 26);
		miPanel.add(btnConsultar);
		
		JButton btnRegistrar = new JButton("Registrar");
		btnRegistrar.setBounds(52, 96, 98, 26);
		miPanel.add(btnRegistrar);
	}
}
