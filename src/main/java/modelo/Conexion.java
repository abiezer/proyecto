package modelo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;

public class Conexion {
	private static Conexion instancia = null;
	
	public static Conexion getInstance() {
		if(instancia == null) {
			instancia = new Conexion();
		}
		return instancia;
	}
	
	private Conexion() {
		return ;	
	}
	
    public static Connection conectarse() {
    	
        // Create a variable for the connection string.
    	final String URL = "jdbc:sqlserver://192.168.1.101:1433;databaseName=curso;";
    	final String USER = "sa";
    	final String PASS = "crecic";
    	
    	try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			Connection con = DriverManager.getConnection(URL, USER, PASS);
			return con;
		} catch (ClassNotFoundException | SQLException e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
    	return null;
    }
    
    public void consulta(String consulta) {
		Connection conn = conectarse();
		PreparedStatement declara;
		try {
			declara=conn.prepareStatement(consulta);
			declara.executeUpdate();
		}catch(SQLException e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
    }
    
    public ResultSet consulta_res(String consulta) {
		Connection conn = conectarse();
		
		Statement declara;
		try {
			declara=conn.createStatement();
			ResultSet respuesta = declara.executeQuery(consulta);
			if(respuesta!=null) {
				return respuesta;
			}
		}catch(SQLException e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
		return null;
    }
    
}
